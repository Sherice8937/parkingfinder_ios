# README #

Parking Finder
by Sherice Sutcliffe

### About ###

* Parking Finder was created from September 5th - September 23rd, 2017 (3 weeks approx.)
* It displays available parking and information about each spot, to assist users in finding good parking.

### Set Up Requirements ###

* Internet/WiFi
* iOS 8+
* Apple Pay (reccommended, NOT required)

### Contact ###

* Repo owner/admin: Sherice Sutcliffe
* Email: sherice.sutcliffe8937@yahoo.com

## Please contact me if you wish to download/use this project in any way
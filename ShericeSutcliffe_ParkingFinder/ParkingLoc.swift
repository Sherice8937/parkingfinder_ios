//
//  ParkingLoc.swift
//  ShericeSutcliffe_ParkingFinder
//
//  Created by Sherice Sutcliffe on 9/12/17.
//  Copyright © 2017 Sherice Sutcliffe. All rights reserved.
//

import Foundation

class ParkingLoc {
    
    var name: String
    var address: String
    var city: String
    var state: String
    var zip: String
    var distance: Int
    var priceFormatted: String
    var apiUrl: String
    var price: Int
    var latitude: Double
    var longitude: Double
    
    
    
    init(name: String, address: String, city: String, state: String, zip: String, distance: Int, priceFormatted: String, apiUrl: String, price: Int, latitude: Double, longitude: Double) {
        
        self.name = name
        self.address = address
        self.city = city
        self.state = state
        self.zip = zip
        self.distance = distance
        self.priceFormatted = priceFormatted
        self.apiUrl = apiUrl
        self.price = price
        self.latitude = latitude
        self.longitude = longitude
        
    }
    
}

//
//  SignUpController.swift
//  ShericeSutcliffe_ParkingFinder
//
//  Created by Sherice Sutcliffe on 9/11/17.
//  Copyright © 2017 Sherice Sutcliffe. All rights reserved.
//

import UIKit
import Firebase
import FBSDKCoreKit
import FBSDKLoginKit

class SignUpController: UIViewController, FBSDKLoginButtonDelegate {

    @IBOutlet weak var emailText: UITextField!
    @IBOutlet weak var passwordText: UITextField!
    @IBOutlet weak var passwordText_02: UITextField!
    @IBOutlet weak var errorLabel: UILabel!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        
        errorLabel.text = ""
        
        // set up Facebook Login button
        let loginButton = FBSDKLoginButton()
        loginButton.delegate = self
        view.addSubview(loginButton)
        loginButton.center.x = 185
        loginButton.center.y = 535
    }

    func emptyFields() {
        errorLabel.text = ""
        errorLabel.isHidden = true
        emailText.text = ""
        passwordText.text = ""
        passwordText_02.text = ""
    }
    
    // function for dismissing the keyboard
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        // has the text field resign as first responder
        view.endEditing(true)
        // tells the responder when the user touches the view
        super.touchesBegan(touches, with: event)
    }
    
    // changes how the return key acts, based on the textField that is the first responder
    // "Next" goes to next textField and "Done" dismisses the keyboard
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        switch textField {
        case emailText:
            passwordText.becomeFirstResponder()
        case passwordText:
            passwordText_02.becomeFirstResponder()
        case passwordText_02:
            passwordText_02.resignFirstResponder()
        default:
            return true
        }
        return true
    }
    
    //
    // MARK: Facebook Login Functions
    //
    
    func loginButton(_ loginButton: FBSDKLoginButton!, didCompleteWith result: FBSDKLoginManagerLoginResult!, error: Error!) {
        if error != nil {
            errorLabel.isHidden = false
            errorLabel.text = "An error occured, please try again"
            return
        } else {
            // get an access token for the signed-in user
            // exchange it for a Firebase credential
            let credential = FacebookAuthProvider.credential(withAccessToken: FBSDKAccessToken.current().tokenString)
            
            // authenticate with Firebase
            Auth.auth().signIn(with: credential) { (user, error) in
                if error != nil {
                    self.errorLabel.isHidden = false
                    self.errorLabel.text = "An error occured, please try again"
                    return
                }
                // user is signed in
                // go to Map
                self.performSegue(withIdentifier: "toMap", sender: self)
                self.emptyFields()
            }
        }
    }
    
    func loginButtonDidLogOut(_ loginButton: FBSDKLoginButton!) {
        // user is now logged out
    }
    
    //
    // MARK: Button Actions
    //
    
    @IBAction func signUpButtonPressed(_ sender: UIButton) {
        errorLabel.isHidden = true
        // check that the email entered is in valid format
        let emailIsValid = checkValidEmail(field: emailText)
        // check that passwords match and are not nil
        let passwordIsValid = checkValidPassword(field_01: passwordText, field_02: passwordText_02)
        
        if emailIsValid == true && passwordIsValid == true {
            // add new account to database
            createNewUser()
            
        } else {
            // display what went wrong
            errorLabel.isHidden = false
        }
    }
    
    
    @IBAction func signInButtonPressed(_ sender: UIButton) {
        // go to Sign In Screen
        dismiss(animated: true, completion: nil)
        emptyFields()
    }
    
    //
    // MARK: Check Fields
    //
    
    func checkValidEmail(field: UITextField) -> Bool {
        
        guard let trimmedText = field.text?.trimmingCharacters(in: .whitespacesAndNewlines) else {
            errorLabel.text = "Email is not valid."
            return false
        }
        
        guard let dataDetector = try? NSDataDetector(types: NSTextCheckingResult.CheckingType.link.rawValue) else {
            errorLabel.text = "Email is not valid."
            return false
        }
        
        let range = NSMakeRange(0, NSString(string: trimmedText).length)
        let allMatches = dataDetector.matches(in: trimmedText, options: [], range: range)
        
        if allMatches.count == 1, allMatches.first?.url?.absoluteString.contains("mailto:") == true {
            return true
        }
        errorLabel.text = "Email is not valid."
        
        
        return false
    }
    
    func checkValidPassword(field_01: UITextField, field_02: UITextField) -> Bool {
        // check that neither field is empty
        if field_01.text != "" && field_02.text != "" {
            // check that they are the same
            if field_02.text == field_01.text {
                return true
            } else {
                self.errorLabel.isHidden = false
                errorLabel.text = "Passwords do not match."
                return false
            }
        } else {
            self.errorLabel.isHidden = false
            errorLabel.text = "Password cannot be blank."
            return false
        }
    }
    
    //
    // MARK: Database functions
    //
    
    func createNewUser() {
        Auth.auth().createUser(withEmail: emailText.text!, password: passwordText.text!) { (user, error) in
            if (error != nil) {
                self.errorLabel.isHidden = false
                self.errorLabel.text = "Error signing up. Please try again"
                print("FIREBASE AUTH ERROR: \n")
                print(error.debugDescription.description)
            } else {
                self.errorLabel.isHidden = true
                // go to Map
                self.performSegue(withIdentifier: "toMap", sender: self)
                self.emptyFields()
            }
        }
    }
}

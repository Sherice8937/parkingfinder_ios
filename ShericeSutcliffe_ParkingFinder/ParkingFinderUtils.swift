//
//  ParkingFinderUtils.swift
//  ShericeSutcliffe_ParkingFinder
//
//  Created by Sherice Sutcliffe on 9/12/17.
//  Copyright © 2017 Sherice Sutcliffe. All rights reserved.
//

import Foundation

class ParkingFinderUtils {
    
    static let URL = "https://api.parkwhiz.com/search/?";
    static let LATITUDE = "lat=";
    static let LONGITUDE = "lng=";
    static let STARTTIME = "start=1501874818&";
    static let ENDTIME = "end=1501885618&";
    static let KEY = "key=fec4fd64d0fc97d65fa170c55e984caaa5e56111";
    
    static var MAP_ACTIVE = false;
    static var DETAILS_SIGN_OUT = false;
}

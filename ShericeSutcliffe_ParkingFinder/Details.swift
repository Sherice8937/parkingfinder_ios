//
//  ViewController.swift
//  ShericeSutcliffe_ParkingFinder
//
//  Created by Sherice Sutcliffe on 9/13/17.
//  Copyright © 2017 Sherice Sutcliffe. All rights reserved.
//

import UIKit
import KCFloatingActionButton
import FirebaseAuth
import MapboxNavigation
import MapboxDirections


class Details: UIViewController, PayPalPaymentDelegate {
    
    @IBOutlet weak var priceText: UILabel!
    @IBOutlet weak var addressText: UILabel!
    @IBOutlet weak var availableSpotsText: UILabel!
    @IBOutlet weak var descriptionText: UITextView!
    @IBOutlet weak var directionsText: UITextView!
    @IBOutlet weak var navigationBarTitle: UINavigationItem!
    
    // variables to hold data passed in from other views
    var spotTitle: String = ""
    var address: String = ""
    var price: String = ""
    var url: String = ""
    
    var currentLat: Double = 41.8781
    var currentLong: Double = -87.6298
    var destLat: Double = 0
    var destLong: Double = 0
    
    var availSpots = ""
    var spotDescription = ""
    var directions = ""
    
    // PayPal variables
    var environment:String = PayPalEnvironmentNoNetwork {
        willSet(newEnvironment) {
            if (newEnvironment != environment) {
                PayPalMobile.preconnect(withEnvironment: newEnvironment)
            }
        }
    }
    
    var payPalConfig = PayPalConfiguration()
    
    override func viewDidAppear(_ animated: Bool) {
        PayPalMobile.preconnect(withEnvironment: environment)
    }
    

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        
        // update UI
        priceText.text = price
        addressText.text = address
        navigationBarTitle.title = spotTitle
        
        // download new data from API
        downloadJSONData()
        
        // set up the FAB button
        // for signing out & getting directions
        setUpFab()
        
        // set up payPalConfig
        payPalConfig.acceptCreditCards = true
        payPalConfig.merchantName = "Parking Finder RESERVATION"  //Give your company name here.
        payPalConfig.merchantPrivacyPolicyURL = URL(string: "https://www.paypal.com/webapps/mpp/ua/privacy-full")
        payPalConfig.merchantUserAgreementURL = URL(string: "https://www.paypal.com/webapps/mpp/ua/useragreement-full")
        
        // set language to English
        payPalConfig.languageOrLocale = Locale.preferredLanguages[0]
        
        // no shipping required
        payPalConfig.payPalShippingAddressOption = .none
    }
    
    func setUpFab() {
        // create the FAB button
        let fab = KCFloatingActionButton()
        // update UI
        fab.buttonColor = UIColor(red:0.53, green:0.47, blue:0.72, alpha: 1.0)
        fab.itemButtonColor = UIColor(red:0.53, green:0.47, blue:0.72, alpha: 1.0)
        fab.buttonImage = #imageLiteral(resourceName: "fabIcon")
        
        // add sign out & list items
        fab.addItem("Sign Out", icon: #imageLiteral(resourceName: "signOutFab_icon")) { (KCFloatingActionButtonItem) in
            // sign out
            let firebaseAuth = Auth.auth()
            do {
                try firebaseAuth.signOut()
                // close fab
                fab.close()
                // so Map knows the user has signed out
                ParkingFinderUtils.DETAILS_SIGN_OUT = true
                // dismiss ViewController
                self.dismiss(animated: true, completion: nil)
                
            } catch let signOutError as NSError {
                print ("Error signing out: %@", signOutError)
            }
        }
        
        fab.addItem("Get Directions", icon: #imageLiteral(resourceName: "directionsFab_icon")) { (KCFloatingActionButtonItem) in
            // get directions
            self.getDirections()
            
            // close fab
            fab.close()
        }
        
        
        // lastly, add it to the view
        self.view.addSubview(fab)
    }
    
    
    
    //
    // MARK: Button Actions
    //
    
    @IBAction func backButtonPressed(_ sender: UIBarButtonItem) {
        // go back to Map
        dismiss(animated: true, completion: nil)
    }
    
    @IBAction func reserveButtonTapped(_ sender: UIButton) {
        // continue to PayPal integration
        self.setUpPayPal()
    }
    
    //
    // MARK: PayPal functions
    //
    
    func setUpPayPal() {
        // remove the first character in the String
        // the first character is $
        price.remove(at: price.startIndex)
        
        let item1 = PayPalItem(name: title!, withQuantity: 1, withPrice: NSDecimalNumber(string: price), withCurrency: "USD", withSku: "Hip-0037")
        
        let items = [item1]
        let subtotal = PayPalItem.totalPrice(forItems: items) // total price of reservation
        
        let payment = PayPalPayment(amount: subtotal, currencyCode: "USD", shortDescription: spotTitle, intent: .sale)
        
        payment.items = items
        
        if (payment.processable) {
            let paymentViewController = PayPalPaymentViewController(payment: payment, configuration: payPalConfig, delegate: self)
            present(paymentViewController!, animated: true, completion: nil)
        }
        else {
            // an error has occurred
            print("Payment not processable: \(payment)")
        }
    }
    
    func payPalPaymentDidCancel(_ paymentViewController: PayPalPaymentViewController) {
        dismiss(animated: true, completion: nil)
    }
    
    func payPalPaymentViewController(_ paymentViewController: PayPalPaymentViewController, didComplete completedPayment: PayPalPayment) {
        dismiss(animated: true, completion: nil)
    }
    
    
    //
    // MARK: Download API Data
    //
    
    func downloadJSONData() {
        // retrieve json data from server and create a default configuration
        let config = URLSessionConfiguration.default
        let session = URLSession(configuration: config)
        
        // set up searchUrl based on current location
        let searchUrl = url + "&" + ParkingFinderUtils.KEY
        
        // validate URL to make sure it's not broken
        if let validURL = URL(string: searchUrl) {
            
            // task that will download whatever is found at validURL as a Data object
            let task = session.dataTask(with: validURL, completionHandler: { (data, response, error) in
                
                // if there is an error, quit
                if error != nil { return }
                
                // check the response, status code, and data
                guard let response = response as? HTTPURLResponse,
                    response.statusCode == 200, //OK
                    let validData = data
                    else { return }
                
                do {
                    if let parkingLocations = try JSONSerialization.jsonObject(with: validData, options: .mutableContainers) as? [String: Any] {
                        
                        let spotDescription = parkingLocations["description"] as? String
                        let spotDirections = parkingLocations["directions"] as? String
                        
                        // loop through each first level item in the array
                        for item in parkingLocations {
                            guard let listings = item.value as? [Any]
                                else { continue }
                            
                            // loop through each item in the parking_listings array
                            for item2 in listings {
                                guard let object2 = item2 as? [String: Any],
                                    let availableSpots = object2["available_spots"] as? Int
                                    else { continue }
                                
                                // update variables
                                self.availSpots = availableSpots.description
                                self.spotDescription = spotDescription!
                                self.directions = spotDirections!
                                
                            }
                        }
                    }
                    DispatchQueue.main.async {
                        // update the UI
                        self.availableSpotsText.text = self.availSpots
                        self.descriptionText.text = self.spotDescription
                        self.directionsText.text = self.directions
                    }
                }
                catch {
                    print("There was the following error: \(error.localizedDescription)")
                }
            })
            task.resume()
        }
    }
    
    //
    // MARK: Directions functions
    //
    
    func getDirections() {
        let origin = Waypoint(coordinate: CLLocationCoordinate2D(latitude: 41.8781, longitude: -87.6298), name: "You")
        let destination = Waypoint(coordinate: CLLocationCoordinate2D(latitude: 41.884837733678, longitude: -87.637228667736), name: title)
        
        let options = RouteOptions(waypoints: [origin, destination], profileIdentifier: .automobileAvoidingTraffic)
        options.routeShapeResolution = .full
        options.includesSteps = true
        
        Directions.shared.calculate(options) { (waypoints, routes, error) in
            guard let route = routes?.first else { return }
            
            let viewController = NavigationViewController(for: route)
            self.present(viewController, animated: true, completion: nil)
        }
    }
}

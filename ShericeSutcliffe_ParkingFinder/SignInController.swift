//
//  ViewController.swift
//  ShericeSutcliffe_ParkingFinder
//
//  Created by Sherice Sutcliffe on 8/2/17.
//  Copyright © 2017 Sherice Sutcliffe. All rights reserved.
//

import UIKit
import Firebase
import FBSDKCoreKit
import FBSDKLoginKit

class SignInController: UIViewController, FBSDKLoginButtonDelegate {
    
    @IBOutlet weak var emailText: UITextField!
    @IBOutlet weak var forgotPasswordLabel: UILabel!
    @IBOutlet weak var errorLabel: UILabel!
    @IBOutlet weak var passwordText: UITextField!
    @IBOutlet weak var fbLoginView: UIView!
    
    var signedIn: Bool = false
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        
        errorLabel.text = ""
        
        // Use Firebase library to configure APIs
        FirebaseApp.configure()
        
        // first check if the user is signed in
        if (Auth.auth().currentUser != nil) {
            // user is signed in
            signedIn = true
        }
        
        // set up Facebook Login button
        let loginButton = FBSDKLoginButton()
        loginButton.delegate = self
        view.addSubview(loginButton)
        loginButton.center.x = 185
        loginButton.center.y = 495
    }
    
    override func viewDidAppear(_ animated: Bool) {
        // check if user is signed in
        if (signedIn == true) {
            // if they are,
            // go to Map
            performSegue(withIdentifier: "toMap", sender: self)
            
            // make false
            // so if user signs out & comes back, they are not redirected
            signedIn = false
        }
        // else, do nothing
    }
    
    func emptyFields() {
        errorLabel.text = ""
        errorLabel.isHidden = true
        emailText.text = ""
        passwordText.text = ""
    }
    
    //
    // MARK: Facebook Login Functions
    //

    func loginButton(_ loginButton: FBSDKLoginButton!, didCompleteWith result: FBSDKLoginManagerLoginResult!, error: Error!) {
        if error != nil {
            errorLabel.isHidden = false
            errorLabel.text = "An error occured, please try again"
            return
        } else {
            // get an access token for the signed-in user
            // exchange it for a Firebase credential
            let credential = FacebookAuthProvider.credential(withAccessToken: FBSDKAccessToken.current().tokenString)
            
            // authenticate with Firebase
            Auth.auth().signIn(with: credential) { (user, error) in
                if error != nil {
                    self.errorLabel.isHidden = false
                    self.errorLabel.text = "An error occured, please try again"
                    return
                }
                // user is signed in
                // go to Map
                self.performSegue(withIdentifier: "toMap", sender: self)
                self.emptyFields()
                
            }
        }
    }
    
    func loginButtonDidLogOut(_ loginButton: FBSDKLoginButton!) {
        // user is now logged out
    }
    
    // function for dismissing the keyboard
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        // has the text field resign as first responder
        view.endEditing(true)
        // tells the responder when the user touches the view
        super.touchesBegan(touches, with: event)
    }
    
    // changes how the return key acts, based on the textField that is the first responder
    // "Next" goes to next textField and "Done" dismisses the keyboard
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        switch textField {
        case emailText:
            passwordText.becomeFirstResponder()
        case passwordText:
            passwordText.resignFirstResponder()
        default:
            return true
        }
        return true
    }


    //
    // MARK: Button Actions
    //
    
    @IBAction func signInButtonPressed(_ sender: UIButton) {
        errorLabel.isHidden = true
        
        Auth.auth().signIn(withEmail: emailText.text!, password: passwordText.text!) { (user, error) in
            if (error != nil) {
                self.errorLabel.isHidden = false
                self.errorLabel.text = "Invalid email or password"
            } else {
                self.errorLabel.isHidden = true
                // go to Map
                self.performSegue(withIdentifier: "toMap", sender: self)
                self.emptyFields()
            }
        }
    }
    
    @IBAction func signUpButtonPressed(_ sender: UIButton) {
        emptyFields()
        // go to Sign Up Screen
        performSegue(withIdentifier: "toSignUp", sender: self)
    }
    
    @IBAction func forgotPasswordPressed(_ sender: UIButton) {
        // show alert dialog asking the user for email
        passwordReset()
    }
    
    //
    // MARK: Password reset functions
    //
    
    func passwordReset() {
        let resetAlert =  UIAlertController(title: "Reset your password", message: "Please enter the email associated with you account", preferredStyle: .alert)
        
        let cancelAction = UIAlertAction(title: "Cancel", style: .destructive, handler: {
            (action) -> Void in
            
            // do nothing
            
        })
        
        let resetAction = UIAlertAction(title: "Reset", style: .default, handler: {
            (action) -> Void in
            
            // field for user to enter their name
            let userEmail = (resetAlert.textFields?[0].text)!
            
            // send email reset code to their email
            Auth.auth().sendPasswordReset(withEmail: userEmail, completion: { (error) in
                if (error != nil) {
                    // display alert telling user something went wrong
                    self.resetError()
                    
                } else {
                    // display alert telling user to check email
                    self.resetSuccess()
                }
            })
            
        })
        
        resetAlert.addTextField { (textField: UITextField) in
            
            textField.keyboardType = .emailAddress
        }
        
        resetAlert.addAction(cancelAction)
        resetAlert.addAction(resetAction)
        present(resetAlert, animated: true, completion: nil)
    }
    
    func resetError() {
        let resetErrorAlert =  UIAlertController(title: "Something went wrong", message: "The email you entered is incorrect or does not exist on an account. Please try again.", preferredStyle: .alert)
        
        let cancelAction = UIAlertAction(title: "Cancel", style: .destructive, handler: {
            (action) -> Void in
            
            // do nothing
            
        })
        
        let retryAction = UIAlertAction(title: "Try Again", style: .destructive, handler: {
            (action) -> Void in
            
            // show passwordReset alert again
            self.passwordReset()
            
        })
        
        resetErrorAlert.addAction(cancelAction)
        resetErrorAlert.addAction(retryAction)
        present(resetErrorAlert, animated: true, completion: nil)
    }
    
    func resetSuccess() {
        let resetSuccessAlert =  UIAlertController(title: "Email sent!", message: "Check the email you provided for a password reset link.", preferredStyle: .alert)
        
        let okayAction = UIAlertAction(title: "Okay", style: .destructive, handler: {
            (action) -> Void in
            
            // do nothing
            
        })
        
        resetSuccessAlert.addAction(okayAction)
        present(resetSuccessAlert, animated: true, completion: nil)
    }
    
}


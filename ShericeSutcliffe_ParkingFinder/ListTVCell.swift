//
//  ListTVCell.swift
//  ShericeSutcliffe_ParkingFinder
//
//  Created by Sherice Sutcliffe on 9/14/17.
//  Copyright © 2017 Sherice Sutcliffe. All rights reserved.
//

import UIKit

class ListTVCell: UITableViewCell {

    @IBOutlet weak var titleText: UILabel!
    @IBOutlet weak var addressText: UILabel!
    @IBOutlet weak var distanceText: UILabel!
    @IBOutlet weak var priceText: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}

//
//  ListTVC.swift
//  ShericeSutcliffe_ParkingFinder
//
//  Created by Sherice Sutcliffe on 9/14/17.
//  Copyright © 2017 Sherice Sutcliffe. All rights reserved.
//

import UIKit

class ListTVC: UITableViewController {
    
    @IBOutlet var parkingTableView: UITableView!
    var currentLat: Double = 0
    var currentLong: Double = 0
    
    var allParking: [ParkingLoc] = []

    override func viewDidLoad() {
        super.viewDidLoad()
    }

    // MARK: - Table view data source

    override func numberOfSections(in tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return 1
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        return allParking.count
    }

    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "listTVCell", for: indexPath) as! ListTVCell

        // Configure the cell...
        cell.addressText.text = allParking[indexPath.row].address
        cell.distanceText.text = allParking[indexPath.row].distance.description
        cell.priceText.text = allParking[indexPath.row].priceFormatted
        cell.titleText.text = allParking[indexPath.row].name

        return cell
    }
    
    @IBAction func backButtonTapped(_ sender: UIBarButtonItem) {
        dismiss(animated: true, completion: nil)
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // check if an item was clicked
        if segue.identifier == "toDetails"{
            if let indexPath = parkingTableView.indexPathsForSelectedRows {
                // get the indexPath from the parkingTableView selected Cell
                let path = indexPath[0] as IndexPath
                // send variables over to Details
                let detailsPage = segue.destination as! Details
                // update variables
                detailsPage.address = allParking[path.row].address
                detailsPage.price = allParking[path.row].priceFormatted
                detailsPage.spotTitle = allParking[path.row].name
                detailsPage.url = allParking[path.row].apiUrl
                detailsPage.currentLat = currentLat
                detailsPage.currentLong = currentLong
                detailsPage.destLat = allParking[path.row].latitude
                detailsPage.destLong = allParking[path.row].longitude
            }
        }
    }
}

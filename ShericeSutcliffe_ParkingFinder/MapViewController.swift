//
//  MapViewController.swift
//  ShericeSutcliffe_ParkingFinder
//
//  Created by Sherice Sutcliffe on 9/12/17.
//  Copyright © 2017 Sherice Sutcliffe. All rights reserved.
//

import UIKit
import CoreLocation
import Mapbox
import KCFloatingActionButton
import FirebaseAuth

class MapViewController: UIViewController, CLLocationManagerDelegate, MGLMapViewDelegate, KCFloatingActionButtonDelegate {
    
    let locationManager = CLLocationManager()
    var currentLocation: CLLocationCoordinate2D? = nil
    var mapView = MGLMapView()
    // variable to hold all parking spaces
    var allParking: [ParkingLoc] = []
    
    let latitude = 41.8781
    let longitude = -87.6298
    
    // variables to send over data about tapped parking spot
    var spotTitle: String = ""
    var url: String = ""
    var address: String = ""
    var price: String = ""
    var currentLat: Double = 0
    var currentLong: Double = 0
    var destLat: Double = 0
    var destLong: Double = 0

    override func viewDidLoad() {
        super.viewDidLoad()
        
        mapView = MGLMapView(frame: view.bounds)
        mapView.autoresizingMask = [.flexibleWidth, .flexibleHeight]
        view.addSubview(mapView)
        
        // set the delegate property of the map view
        mapView.delegate = self
        
        // ask for current location from the user
        locationManager.requestAlwaysAuthorization()
        
        // get access
        locationManager.requestWhenInUseAuthorization()
        
        if CLLocationManager.locationServicesEnabled() {
            locationManager.delegate = self
            locationManager.desiredAccuracy = kCLLocationAccuracyBest
            locationManager.startUpdatingLocation()
            // download API data
            downloadJSONData()
        }
        
        // set up the FAB button
        // for signing out & going to ListTVC
        setUpFab()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        // check if the user has signed out
        if (ParkingFinderUtils.DETAILS_SIGN_OUT == true) {
            // make details sign out false
            // so user can sign out from that way again
            ParkingFinderUtils.DETAILS_SIGN_OUT = false
            // go back
            dismiss(animated: false, completion: nil)
        }
    }
    
    func setUpFab() {
        // create the FAB button
        let fab = KCFloatingActionButton()
        // update UI
        fab.buttonColor = UIColor(red:0.53, green:0.47, blue:0.72, alpha: 1.0)
        fab.itemButtonColor = UIColor(red:0.53, green:0.47, blue:0.72, alpha: 1.0)
        fab.buttonImage = #imageLiteral(resourceName: "fabIcon")
        
        // add sign out & list items
        fab.addItem("Sign Out", icon: #imageLiteral(resourceName: "signOutFab_icon")) { (KCFloatingActionButtonItem) in
            // sign out
            let firebaseAuth = Auth.auth()
            do {
                try firebaseAuth.signOut()
                // close fab
                fab.close()
                
                // dismiss ViewController
                self.dismiss(animated: true, completion: nil)
                
            } catch let signOutError as NSError {
                print ("Error signing out: %@", signOutError)
            }
        }
        
        fab.addItem("View as List", icon: #imageLiteral(resourceName: "listFab_icon")) { (KCFloatingActionButtonItem) in
            // go to ListTVC
            self.performSegue(withIdentifier: "toList", sender: self)
            
            // close fab
            fab.close()
        }
        
        
        // lastly, add it to the view
        self.view.addSubview(fab)
    }
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        // update user's current location
        if (currentLocation == nil) {
            currentLocation = manager.location?.coordinate
            // update the map
            //updateMap()
        }
    }
    
    //
    // MARK: Map functions
    //
    
    func updateMap() {
        // set the map’s center coordinate and zoom level
        mapView.setCenter(CLLocationCoordinate2D(latitude: currentLocation!.latitude, longitude: currentLocation!.longitude), zoomLevel: 14, animated: true)
        // set up the current location marker
        let currentLocMarker = MGLPointAnnotation()
        currentLocMarker.coordinate = CLLocationCoordinate2D(latitude: currentLocation!.latitude, longitude: currentLocation!.longitude)
        currentLocMarker.title = "You"
        // add the marker to the map.
        mapView.addAnnotation(currentLocMarker)
    }
    
    func updateChicagoMap() {
        // set the map’s center coordinate and zoom level
        mapView.setCenter(CLLocationCoordinate2D(latitude: latitude, longitude: longitude), zoomLevel: 14, animated: true)
        // set up the current location marker
        let currentLocMarker = MGLPointAnnotation()
        currentLocMarker.coordinate = CLLocationCoordinate2D(latitude: latitude, longitude: longitude)
        currentLocMarker.title = "You"
        // add the marker to the map.
        mapView.addAnnotation(currentLocMarker)
        // display annotation so user knows this marker is them
        mapView.selectAnnotation(currentLocMarker, animated: true)
    }
    
    // use the default marker
    func mapView(_ mapView: MGLMapView, viewFor annotation: MGLAnnotation) -> MGLAnnotationView? {
        return nil
    }
    
    // allow callout view to appear when an annotation is tapped
    func mapView(_ mapView: MGLMapView, annotationCanShowCallout annotation: MGLAnnotation) -> Bool {
        return true
    }
    
    func mapView(_ mapView: MGLMapView, imageFor annotation: MGLAnnotation) -> MGLAnnotationImage? {
        let currentTitle: String? = annotation.title!!
        
        // first make sure there is a title available
        if (currentTitle != nil) {
            // check if the current Map Marker is the user's current location
            if (currentTitle == "You") {
                // if it is, return a different image
                let image = UIImage(named: "currentLoc_icon")!
                
                let annotationImage = MGLAnnotationImage(image: image, reuseIdentifier: "currentLoc_icon")
                
                return annotationImage
            }
        }
        
        return nil
    }
    
    func addMapMarkers() {
        for spot in allParking {
            let spotMarker = MGLPointAnnotation()
            spotMarker.coordinate = CLLocationCoordinate2D(latitude: spot.latitude, longitude: spot.longitude)
            spotMarker.title = spot.name
            spotMarker.subtitle = spot.distance.description + " feet away" + " | " + spot.priceFormatted
            // add the marker to the map.
            mapView.addAnnotation(spotMarker)
        }
        
        updateChicagoMap()
    }
    
    func mapView(_ mapView: MGLMapView, tapOnCalloutFor annotation: MGLAnnotation) {
        // find out which spot was tapped, based on longitude and latitude
        for spot in allParking {
            if (spot.longitude == annotation.coordinate.longitude) {
                if (spot.latitude == annotation.coordinate.latitude) {
                    // update variables
                    spotTitle = spot.name
                    url = spot.apiUrl
                    address = spot.address
                    price = spot.priceFormatted
                    destLat = spot.latitude
                    destLong = spot.longitude
                    // if both coordinates are matching, go to Details Screen with data
                    performSegue(withIdentifier: "toDetails", sender: self)
                }
            }
        }
    }
    
    //
    // MARK: Navigation
    //
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if let detailsVC = segue.destination as? Details {
            // send over data from tapped parkingSpot
            detailsVC.spotTitle = spotTitle
            detailsVC.url = url
            detailsVC.address = address
            detailsVC.price = price
            detailsVC.currentLat = latitude
            detailsVC.currentLong = longitude
            detailsVC.destLat = destLat
            detailsVC.destLong = destLong
        }
        
        if let listViewVC = segue.destination as? ListTVC {
            // TODO: send over data from allParking
            listViewVC.allParking = allParking
            listViewVC.currentLat = latitude
            listViewVC.currentLong = longitude
        }
    }
    
    
    //
    // MARK: Download API Data
    //
    
    func downloadJSONData() {
        // retrieve json data from server and create a default configuration
        let config = URLSessionConfiguration.default
        let session = URLSession(configuration: config)
        
        // set up searchUrl based on current location
        // TODO: CHANGE TO CURRENTLOCATION.LATITUDE & LONGITUDE
        let searchUrl = ParkingFinderUtils.URL +
            ParkingFinderUtils.LATITUDE + latitude.description + "&" +
            ParkingFinderUtils.LONGITUDE + longitude.description + "&" +
            ParkingFinderUtils.STARTTIME +
            ParkingFinderUtils.ENDTIME +
            ParkingFinderUtils.KEY
        
        // validate URL to make sure it's not broken
        if let validURL = URL(string: searchUrl) {
            
            // task that will download whatever is found at validURL as a Data object
            let task = session.dataTask(with: validURL, completionHandler: { (data, response, error) in
                
                // if there is an error, quit
                if error != nil { return }
                
                // check the response, status code, and data
                guard let response = response as? HTTPURLResponse,
                    response.statusCode == 200, //OK
                    let validData = data
                    else { return }
                
                do {
                    if let parkingLocations = try JSONSerialization.jsonObject(with: validData, options: .mutableContainers) as? [String: Any] {
                        
                        // loop through each first level item in the array
                        for item in parkingLocations {
                            guard let listings = item.value as? [Any]
                                else { continue }
                            
                            // loop through each item in the parking_listings array
                            for item2 in listings {
                                guard let object2 = item2 as? [String: Any],
                                    let name = object2["location_name"] as? String,
                                    let address = object2["address"] as? String,
                                    let city = object2["city"] as? String,
                                    let state = object2["state"] as? String,
                                    let zip = object2["zip"] as? String,
                                    let distance = object2["distance"] as? Int,
                                    let priceFormatted = object2["price_formatted"] as? String,
                                    let price = object2["price"] as? Int,
                                    let apiUrl = object2["api_url"] as? String,
                                    let latitude = object2["lat"] as? Double,
                                    let longitude = object2["lng"] as? Double
                                    else { continue }
                                
                                // append these objects to the array
                                self.allParking.append(ParkingLoc(name: name, address: address, city: city, state: state, zip: zip, distance: distance, priceFormatted: priceFormatted, apiUrl: apiUrl, price: price, latitude: latitude, longitude: longitude))
                            }
                        }
                    }
                    DispatchQueue.main.async {
                        // update the map
                        self.addMapMarkers()
                    }
                }
                catch {
                    print("There was the following error: \(error.localizedDescription)")
                }
            })
            task.resume()
        }
    }
}
